///*
// * Name: Yanzhi Wang
// */
//
//import java.util.ArrayDeque;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
///**
// * Your solution goes in this class.
// * <p>
// * Please do not modify the other files we have provided for you, as we will use
// * our own versions of those files when grading your project. You are
// * responsible for ensuring that your solution works with the original version
// * of all the other files we have provided for you.
// * <p>
// * That said, please feel free to add additional files and classes to your
// * solution, as you see fit. We will use ALL of your additional files when
// * grading your solution.
// */
//public class Program1 extends AbstractProgram1 {
//    public static ArrayList<ArrayList<Integer>> allAdvisorPreferenceList = new ArrayList<>();
//
//    private static class StudentInformation {
//
//        private int studentName;
//        private Double GPA;
//        private Double distance;
//
//        public StudentInformation(int studentName, Double GPA, Double distance) {
//            this.studentName = studentName;
//            this.GPA = GPA;
//            this.distance = distance;
//        }
//
//        public int getStudentName() {
//            return studentName;
//        }
//
//        public void setStudentName(int studentName) {
//            this.studentName = studentName;
//        }
//
//        public Double getGPA() {
//            return GPA;
//        }
//
//        public void setGPA(Double GPA) {
//            this.GPA = GPA;
//        }
//
//        public Double getDistance() {
//            return distance;
//        }
//
//        public void setDistance(Double distance) {
//            this.distance = distance;
//        }
//    }
//
//    // Time complexity n n log n
//    public static void buildAdvisorPreferenceList(Matching marriage) {
//        List<StudentInformation> studentList = new ArrayList<>();
//        ArrayList<Integer> iThAdvisorPreferenceList = new ArrayList<>();
//        ArrayList<Double> studentGPAs = marriage.getStudentGPAs();
//        ArrayList<Coordinate> studentLocations = marriage.getStudentLocations();
//        ArrayList<Coordinate> adviserLocations = marriage.getAdviserLocations();
//        Double distance;
//        for (int iThAdvisor = 0; iThAdvisor < marriage.getNumberOfAdvisers(); iThAdvisor++) {
//            studentList.clear();
//            iThAdvisorPreferenceList.clear();
//            // Time complexity n
//            for (int iThStudent = 0; iThStudent < marriage.getNumberOfStudents(); iThStudent++) {
//                distance = Math.sqrt(Math.pow(
//                        (adviserLocations.get(iThStudent).x -
//                                studentLocations.get(iThStudent).x), 2) +
//                        Math.pow(
//                                (adviserLocations.get(iThAdvisor).y -
//                                        studentLocations.get(iThStudent).y), 2));
//                studentList.add(new StudentInformation(iThStudent, studentGPAs.get(iThAdvisor), distance));
//            }
//
//            // Time complexity n log n
//            Collections.sort(studentList, Comparator.comparing(StudentInformation::getGPA).reversed()
//                    .thenComparing(StudentInformation::getDistance));
//
//            // Time complexity n
//            for (int i = 0; i < studentList.size(); i++) {
//                iThAdvisorPreferenceList.add(i, studentList.get(i).studentName);
//            }
//
//            allAdvisorPreferenceList.add(iThAdvisor, iThAdvisorPreferenceList);
//
//        }
//    }
//
//    /**
//     * Determines whether a candidate Matching represents a solution to the
//     * Stable Marriage problem. Study the description of a Matching in the
//     * project documentation to help you with this.
//     */
//    public boolean isStableMatching(Matching marriage) {
//        /* TODO implement this function */
//        return false; /* TODO remove this line */
//    }
//
//    /**
//     * Determines a solution to the Stable Marriage problem from the given input
//     * set. Study the project description to understand the variables which
//     * represent the input to your solution.
//     *
//     * @return A stable Matching.
//     */
//
//    /*
//    *   // the student preference list is given, but the advisor preference list is not given
//        // we will use the build_advisor_preference_list function to get the advisor preference
//        // list. We will not call the function in student_advisor function
//        studentAdvisor () {
//            Initially all s in S and a in A are free
//            // you can use stack to store free students. You can push student in as well as pop student out
//            // In the code, let's use queue to store free students
//              While there is a s who is free and has not applied to every a in A {
//                Let a be the highest-ranked in s’s preference list to whom s has not yet matched
//                if a is free {
//                    then a accepts the application, so (s’,a) is matched
//                }
//                Else advisor a has accepted student s’ {
//                    if a prefers the currently matched student s’ to s {
//                        a rejects s’s application, so s’ is still matched and s is not matched
//                    }
//                    Else a prefers s to s’ {
//                        a accepts s’s application, so s is matched and s’ is not matched
//                    }
//                }
//            }
//            Returns the matches between s in S and a in A
//        }
//    * */
//    public Matching stableMarriageGaleShapley(Matching marriage) {
//        ArrayList<Integer> studentMatching = marriage.getStudentMatching();
//        ArrayList<Integer> matchForAdvisor = new ArrayList<>();
//        ArrayList<Integer> matchForStudent = new ArrayList<>();
//        int numberOfStudent = marriage.getNumberOfStudents();
//
//        // Initially all s in S and a in A are free
//        for (int iThCurrentStudent = 0; iThCurrentStudent < numberOfStudent; iThCurrentStudent++) {
//            matchForStudent.add(iThCurrentStudent, -1);
//        }
//        for (int iThAdvisor = 0; iThAdvisor < numberOfStudent; iThAdvisor++) {
//            matchForAdvisor.add(iThAdvisor, -1);
//        }
//
//        // store free students in queue
//        ArrayDeque<Integer> freeStudentsQueue = new ArrayDeque<Integer>();
//        for (int iThCurrentStudent = 0; iThCurrentStudent < numberOfStudent; iThCurrentStudent++) {
//            // enqueue element
//            // add element to the last
//            freeStudentsQueue.add(iThCurrentStudent);
//        }
//
//        // count the times the student applied
//        ArrayList<Integer> countApply = new ArrayList<Integer>();
//        for (int iThCurrentStudent = 0; iThCurrentStudent < numberOfStudent; iThCurrentStudent++) {
//            countApply.add(iThCurrentStudent, -1);
//        }
//
//        ArrayList<ArrayList<Integer>> allStudentPreferenceList = marriage.getStudentPreference();
//
//        while (true) {
//            // dequeue element
//            // get the first element
//            int studentComingToApply = freeStudentsQueue.pop();
//            // update the times the student applied
//            int prevApplyTimes = countApply.get(studentComingToApply);
//            int currentApplyTimes = prevApplyTimes + 1;
//            countApply.set(studentComingToApply, currentApplyTimes);
//            // find the nearly top advisor in the student's preference list
//            ArrayList<Integer> iThStudentPreferenceList = allStudentPreferenceList.get(studentComingToApply);
//            int nearlyTopAdvisor = iThStudentPreferenceList.get(currentApplyTimes);
//            // if advisor is free
//            if (matchForAdvisor.get(nearlyTopAdvisor) != -1) {
//                // then a accepts the application
//                // match student to advisor
//                matchForStudent.set(studentComingToApply, nearlyTopAdvisor);
//                // match advisor to student
//                matchForAdvisor.set(nearlyTopAdvisor, studentComingToApply);
//            }
//
//            // if there is no free student in the queue, jump out the while loop
//            if (freeStudentsQueue.isEmpty()) {
//                break;
//            }
//        }
//
//        return null; /* TODO remove this line */
//
//    }
//}
//
