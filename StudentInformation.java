public class StudentInformation {
    private int studentName;
    private int GPA;
    private int distance;

    public StudentInformation() {

    }

    public int getStudentName() {
        return studentName;
    }

    public void setStudentName(int studentName) {
        this.studentName = studentName;
    }

    public int getGPA() {
        return GPA;
    }

    public void setGPA(int GPA) {
        this.GPA = GPA;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Report{" +
                "student='" + studentName + '\'' +
                ", GPA='" + GPA + '\'' +
                ", distance='" + distance + '\'' +
                '}';
    }
}

