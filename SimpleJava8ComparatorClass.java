// https://stackoverflow.com/questions/4258700/collections-sort-with-multiple-fields
// java sort multiple element
// java sort multiple field
// java sort multiple attributes

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SimpleJava8ComparatorClass {

    public static void main(String[] args) {
        List<StudentInformation> studentList = new ArrayList<>();
        studentList.add(new StudentInformation(1, 1, 5));
        studentList.add(new StudentInformation(2, 2, 10));
        studentList.add(new StudentInformation(3, 3, 20));
        studentList.add(new StudentInformation(4, 3, 5));
        studentList.add(new StudentInformation(5, 3, 1));
        studentList.add(new StudentInformation(6, 4, 10));

        Collections.sort(studentList, Comparator.comparing(StudentInformation::getGPA).reversed()
                .thenComparing(StudentInformation::getDistance));

        for (int i = 0; i < studentList.size(); i++) {
            System.out.println(studentList.get(i).studentName + " ");
        }

//        System.out.println("post-sorting");
//        System.out.println(studentList.get(1).studentName);
    }

    private static class StudentInformation {

        private int studentName;
        private int GPA;
        private int distance;

        public StudentInformation(int studentName, int GPA, int distance) {
            this.studentName = studentName;
            this.GPA = GPA;
            this.distance = distance;
        }

        public int getStudentName() {
            return studentName;
        }

        public void setStudentName(int studentName) {
            this.studentName = studentName;
        }

        public int getGPA() {
            return GPA;
        }

        public void setGPA(int GPA) {
            this.GPA = GPA;
        }

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        @Override
        public String toString() {
            return "Report{" +
                    "student='" + studentName + '\'' +
                    ", GPA='" + GPA + '\'' +
                    ", distance='" + distance + '\'' +
                    '}';
        }
    }
}
