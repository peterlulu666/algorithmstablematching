/*
 * Name: Yanzhi Wang
 */

import java.util.ArrayList;
import java.util.Stack;

/**
 * Your solution goes in this class.
 * <p>
 * Please do not modify the other files we have provided for you, as we will use
 * our own versions of those files when grading your project. You are
 * responsible for ensuring that your solution works with the original version
 * of all the other files we have provided for you.
 * <p>
 * That said, please feel free to add additional files and classes to your
 * solution, as you see fit. We will use ALL of your additional files when
 * grading your solution.
 */
public class Program1 extends AbstractProgram1 {
    /**
     * Determines whether a candidate Matching represents a solution to the
     * Stable Marriage problem. Study the description of a Matching in the
     * project documentation to help you with this.
     */

    // Inside Program1.java is a placeholder for a verify function called isStableMatching().
    // Implement this function to complete the Brute Force algorithm
    public boolean isStableMatching(Matching marriage) {
        // number of advisors
        int numberOfAdvisors = marriage.getNumberOfAdvisers();
        // number of students
        int numberOfStudents = marriage.getNumberOfStudents();
        // if number of student is smaller, number of couples is number of student
        // if number of advisor is smaller. number of couples is number of advisor
        // number of couples
        int numberOfStudentAdvisorPairs = Math.min(numberOfAdvisors, numberOfStudents);

        // store students' preference
        ArrayList<ArrayList<Integer>> studentPreference = marriage.getStudentPreference();
        // store student matching
        ArrayList<Integer> studentMatching = marriage.getStudentMatching();

        // In this lab you will be using double values to represent a student’s GPA
        // store students' GPAs
        // Normally this would make it difficult to compare the values directly
        // Let's use compareTo() to compare GPA
        ArrayList<Double> studentGPAs = marriage.getStudentGPAs();
        Double betterStudentGPA;
        Double currentStudentGPA;

        // store student locations
        ArrayList<Coordinate> studentLocations = marriage.getStudentLocations();
        // store adviser locations
        ArrayList<Coordinate> adviserLocations = marriage.getAdviserLocations();
        // distance between better student and advisor
        Double betterStudentDistance;
        // distance between current student and advisor
        Double currentStudentDistance;

        for (int currentStudent = 0; currentStudent < numberOfStudentAdvisorPairs; currentStudent++) {
            // we need to find who is matched to the i th currentStudent
            // the i th currentStudent is matched to this advisor
            int currentAdvisor = studentMatching.get(currentStudent);

            // the i th student's list of preference
            ArrayList<Integer> iThStudentPreferenceList = studentPreference.get(currentStudent);
            // we need to find where is the matched advisor on the i th student's preference list
            int matchedAdvisorPosition = iThStudentPreferenceList.indexOf(currentAdvisor);

            // check if there is any advisor before the matched advisor in student’s list of preference
            // if there is no advisor before the matched advisor in student’s list of preference
            if (matchedAdvisorPosition <= 0) {
                continue;
            }

            // if there exists advisor before the matched advisor in student’s list of preference
            // we will let the student apply to advisors before the matched advisor
            // we will let the student apply to the first advisor before the matched advisor,
            // then the second advisor before the matched advisor,
            // then the n th advisor before the matched advisor and the advisor at index 0
            // we will find if the student can do better than the matched advisor
            for (int betterAdvisorPosition = matchedAdvisorPosition - 1; betterAdvisorPosition >= 0; betterAdvisorPosition--) {
                // we need to find who is before the matched advisor
                int betterAdvisor = iThStudentPreferenceList.get(betterAdvisorPosition);
                // we need to find who is matched to this advisor
                int betterStudent = studentMatching.indexOf(betterAdvisor);

                // we will compare the betterStudent GPA with the currentStudent GPA
                betterStudentGPA = studentGPAs.get(betterStudent);
                currentStudentGPA = studentGPAs.get(currentStudent);

                // if the current student has lower GPA,
                // the current student cannot do better.
                // So we cannot conclude that if the matching is stable or not.

                // if the current student has higher GPA,
                // the current student can do better,
                // so we can conclude that the matching is not stable.
                if (betterStudentGPA.compareTo(currentStudentGPA) < 0) {
                    return false;
                }

                // If there’s a tie between two student’s GPAs
                // the advisor will prefer the student closest to themselves

                // if the students have the same GPA
                else if (betterStudentGPA.compareTo(currentStudentGPA) == 0) {
                    // To find the distance between an advisor and a student, you should treat it as an x-y plot and
                    // look for the distance between two points i.e. use the distance
                    // formula d = sqrt((x2−x1)^2+(y2−y1)^2)

                    // distance between better student and advisor
                    betterStudentDistance = Math.sqrt(Math.pow(
                            (adviserLocations.get(betterAdvisor).x -
                                    studentLocations.get(betterStudent).x), 2) +
                            Math.pow(
                                    (adviserLocations.get(betterAdvisor).y -
                                            studentLocations.get(betterStudent).y), 2));

                    // distance between current student and advisor
                    currentStudentDistance = Math.sqrt(Math.pow(
                            (adviserLocations.get(betterAdvisor).x -
                                    studentLocations.get(currentStudent).x), 2) +
                            Math.pow(
                                    (adviserLocations.get(betterAdvisor).y -
                                            studentLocations.get(currentStudent).y), 2));

                    // then advisor will compare the distance

                    // if the current student has higher distance value,
                    // the current student cannot do better.
                    // So we cannot conclude that if the matching is stable or not.

                    // if the current student has lower distance value,
                    // the current student can do better,
                    // so we can conclude that the matching is not stable.
                    if ((betterStudentDistance).compareTo(currentStudentDistance) > 0) {
                        return false;
                    }
                }
            }
        }

        return true;

    }

    /**
     * Determines a solution to the Stable Marriage problem from the given input
     * set. Study the project description to understand the variables which
     * represent the input to your solution.
     *
     * @return A stable Matching.
     */
    public Matching stableMarriageGaleShapley(Matching marriage) {
        // number of advisors
        int numberOfAdvisors = marriage.getNumberOfAdvisers();
        // number of students
        int numberOfStudents = marriage.getNumberOfStudents();
        // if number of student is smaller, number of matching is number of student
        // if number of advisor is smaller. number of matching is number of advisor
        // number of matching
        int numberOfStudentAdvisorPair = Math.min(numberOfAdvisors, numberOfStudents);

        // Initially all s in S and a in A are free
        // store student match result
        ArrayList<Integer> matchForStudent = new ArrayList<>();
        for (int iThCurrentStudent = 0; iThCurrentStudent < numberOfStudentAdvisorPair; iThCurrentStudent++) {
            // if the element is -1, it means that the i th student is not matched to advisor
            // if the element is 0, it means that the i th student is matched to advisor at index 0
            // if the element is n, it means that the i th student is matched to advisor at index n
            matchForStudent.add(iThCurrentStudent, -1);
        }
        // store advisor match result
        ArrayList<Integer> matchForAdvisor = new ArrayList<>();
        for (int iThAdvisor = 0; iThAdvisor < numberOfStudentAdvisorPair; iThAdvisor++) {
            // if the element is -1, it means that the i th advisor is not matched to student
            // if the element is 0, it means that the i th advisor is matched to student at index 0
            // if the element is n, it means that the i th advisor is matched to student at index n
            matchForAdvisor.add(iThAdvisor, -1);
        }

        // store free students in stack
        // we would use stack to store free students
        // if the student applied to advisor but is still not matched
        // the student will go to the stack top
        // we will continue to let the student apply to advisor
        Stack<Integer> freeStudentStack = new Stack<>();
        for (int iThCurrentStudent = 0; iThCurrentStudent < numberOfStudentAdvisorPair; iThCurrentStudent++) {
            // the i th student is free
            // push the i th student into the freeStudentStack
            freeStudentStack.push(iThCurrentStudent);
        }

        // count the times the student applied
        // store number of the application
        // count how many application the student send
        ArrayList<Integer> numberOfApplication = new ArrayList<>();
        for (int iThCurrentStudent = 0; iThCurrentStudent < numberOfStudentAdvisorPair; iThCurrentStudent++) {
            // if the element is -1, it means that the i th student has not applied to any advisor
            // if the element is 0, it means that the i th student has applied once
            // if the element is 1, it means that the i th student has applied n + 1 times
            numberOfApplication.add(iThCurrentStudent, -1);
        }

        // store all students' preference list
        ArrayList<ArrayList<Integer>> allStudentPreferenceList = marriage.getStudentPreference();

        // the i th student is on the top of stack
        int iThStudentComingToApply;

        // count apply times
        int prevApplyTimes;
        int currentApplyTimes;

        // the n th nearly top advisor whom the student has not applied
        int nearlyTopAdvisor;

        // store the currently matched student
        int matchedStudent;

        // In this lab you will be using double values to represent a student’s GPA
        // store students' GPAs
        // Normally this would make it difficult to compare the values directly
        // Let's use compareTo() to compare GPA
        ArrayList<Double> studentGPAs = marriage.getStudentGPAs();
        Double matchedStudentGPA;
        Double studentComingToApplyGPA;

        // store student locations
        ArrayList<Coordinate> studentLocations = marriage.getStudentLocations();

        // store adviser locations
        ArrayList<Coordinate> adviserLocations = marriage.getAdviserLocations();

        // distance between the currently matched student and advisor
        Double studentMatchedDistance;

        // the distance between the student coming to apply and advisor
        Double studentComingApplyDistance;

        while (true) {
            // the i th student is on the top of stack
            // the pop() function will remove it from the freeStudentStack and return the value
            // store the i th student value into the iThStudentComingToApply
            // in the first iteration of the while loop
            // it is the i th student
            // in the second iteration of the while loop
            // it is either the i th student or the dumped student
            iThStudentComingToApply = freeStudentStack.pop();
            // the student popped from stack will send another application
            // update numberOfApplication
            prevApplyTimes = numberOfApplication.get(iThStudentComingToApply);
            currentApplyTimes = prevApplyTimes + 1;
            numberOfApplication.set(iThStudentComingToApply, currentApplyTimes);
            // store the i th student's list of preference
            ArrayList<Integer> iThStudentPreferenceList = allStudentPreferenceList.get(iThStudentComingToApply);
            // student choose a nearly-top advisor whom the student has not applied.
            // student will not be able to do better than this advisor.
            // if the currentApplyTimes is 0,
            // it means that the i th student has not applied,
            // so the student will apply to the first top advisor who is at the index 0.
            // if the currentApplyTimes is 1,
            // it means that the i th student applied once but is still not matched to advisor,
            // so the student will apply to the second top advisor who is at the index 1.
            // if the currentApplyTimes is n,
            // it means that the i th student applied n times but is still not matched to advisor,
            // so the student will apply to the n + 1 th top advisor who is at
            // the index n, where n is between 0 and numberOfStudentAdvisorPair.
            nearlyTopAdvisor = iThStudentPreferenceList.get(currentApplyTimes);
            // if the advisor the student is applying to is not matched
            if (matchForAdvisor.get(nearlyTopAdvisor) == -1) {
                // the student is matched to the advisor
                matchForStudent.set(iThStudentComingToApply, nearlyTopAdvisor);
                // then the advisor is matched to the student
                matchForAdvisor.set(nearlyTopAdvisor, iThStudentComingToApply);
            }
            // if advisor is matched,
            // advisor will compare the student coming to apply with the currently matched student
            else if (matchForAdvisor.get(nearlyTopAdvisor) != -1) {
                // we will compare the matchedStudent GPA with the iThStudentComingToApply GPA
                // store the currently matched student
                matchedStudent = matchForAdvisor.get(nearlyTopAdvisor);
                // store the currently matched student GPA
                matchedStudentGPA = studentGPAs.get(matchedStudent);
                // store iThStudentComingToApply GPA
                studentComingToApplyGPA = studentGPAs.get(iThStudentComingToApply);
                // if the currently matched student has a lower GPA
                if (matchedStudentGPA.compareTo(studentComingToApplyGPA) < 0) {
                    // then advisor dumps the currently matched student
                    // the currently matched student is free
                    freeStudentStack.push(matchedStudent);
                    // then advisor is matched to the proposing student
                    matchForAdvisor.set(nearlyTopAdvisor, iThStudentComingToApply);
                    // the student coming to apply is matched to advisor
                    matchForStudent.set(iThStudentComingToApply, nearlyTopAdvisor);
                }
                // if the currently matched student has a higher GPA
                else if (matchedStudentGPA.compareTo(studentComingToApplyGPA) > 0) {
                    // then advisor prefers the currently matched student
                    // the student coming to apply is still free
                    freeStudentStack.push(iThStudentComingToApply);
                }

                // If there’s a tie between two student’s GPAs
                // the advisor will prefer the student closest to themselves

                // if the currently matched student and the student coming to apply have same GPA
                else if (matchedStudentGPA.compareTo(studentComingToApplyGPA) == 0) {
                    // To find the distance between an advisor and a student, you should treat it as an x-y plot and
                    // look for the distance between two points i.e. use the distance
                    // formula d = sqrt((x2−x1)^2+(y2−y1)^2)

                    // distance between the currently matched student and advisor
                    studentMatchedDistance = Math.sqrt(Math.pow(
                            (adviserLocations.get(nearlyTopAdvisor).x -
                                    studentLocations.get(matchedStudent).x), 2) +
                            Math.pow(
                                    (adviserLocations.get(nearlyTopAdvisor).y -
                                            studentLocations.get(matchedStudent).y), 2));

                    // the distance between the student coming to apply and advisor
                    studentComingApplyDistance = Math.sqrt(Math.pow(
                            (adviserLocations.get(nearlyTopAdvisor).x -
                                    studentLocations.get(iThStudentComingToApply).x), 2) +
                            Math.pow(
                                    (adviserLocations.get(nearlyTopAdvisor).y -
                                            studentLocations.get(iThStudentComingToApply).y), 2));

                    // if they have the same GPA
                    // then advisor will compare the distance

                    // if studentComingApplyDistance is smaller
                    if (studentMatchedDistance.compareTo(studentComingApplyDistance) > 0) {
                        // then advisor prefers the proposing student
                        // so advisor dumps the currently matched student
                        // the currently matched student is free
                        freeStudentStack.push(matchedStudent);
                        // then advisor is matched to the proposing student
                        matchForAdvisor.set(nearlyTopAdvisor, iThStudentComingToApply);
                        // the proposing student is matched to advisor
                        matchForStudent.set(iThStudentComingToApply, nearlyTopAdvisor);
                    }

                    // if studentMatchedDistance is smaller
                    else if (studentMatchedDistance.compareTo(studentComingApplyDistance) < 0) {
                        // then advisor prefers the currently matched student
                        // the proposing student is still free
                        freeStudentStack.push(iThStudentComingToApply);
                    }
                }
            }

            // if there is no free student in the freeStudentStack, jump out the while loop
            if (freeStudentStack.empty()) {
                break;
            }

        }

        // store student advisor match result
        ArrayList<Integer> studentAdvisorMatching = new ArrayList<>();
        // use the for loop to add the student and the corresponding advisor to the studentAdvisorMatching
        for (int iThStudent = 0; iThStudent < numberOfStudentAdvisorPair; iThStudent++) {
            // the index is the i th student
            // the element is the corresponding advisor
            studentAdvisorMatching.add(iThStudent, matchForStudent.get(iThStudent));
        }

        // The results of your algorithm should be stored in this field either by
        // calling setResidentMatching(<your solution>) or
        // constructing a new Matching(marriage, <your solution>),
        // where marriage is the Matching we pass into the function
        marriage.setResidentMatching(studentAdvisorMatching);
        return marriage;
//        return new Matching(marriage, studentAdvisorMatching);

    }
}





